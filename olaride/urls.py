from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls import url, include
from rest_framework import routers
from bookings.views import BookingViewSet, CheckInView,CheckOutView
from regions.views import RegionViewSet
from rides.views import BusCheckinView

router = routers.DefaultRouter()
router.register(r'bookings', BookingViewSet)
router.register(r'regions', RegionViewSet)

urlpatterns = patterns('',
                       url(r'^', include(router.urls)),
                       url(r'^check-in/bus/(?P<bus_id>[\d]+)/(?P<address_id>[\d]+)', BusCheckinView.as_view(), ),
                       url(r'^check-in/(?P<booking_id>[\d]+)/(?P<ride_id>[\d]+)', CheckInView.as_view(), ),
                       url(r'^check-out/(?P<booking_id>[\d]+)/(?P<ride_id>[\d]+)', CheckOutView.as_view(), ),
                       url(r'^admin/', include(admin.site.urls))
)

