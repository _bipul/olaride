from django.db import models

# Create your models here.
class Regions(models.Model):
    title = models.CharField(max_length=100)
    num_cabs = models.PositiveIntegerField(default=0)
    num_request = models.PositiveIntegerField(default=0)
    center_lat = models.DecimalField(max_digits=15,decimal_places=10,blank=True,null=True)
    center_long = models.DecimalField(max_digits=15,decimal_places=10,blank=True,null=True)

    def __unicode__(self):
        return self.title

