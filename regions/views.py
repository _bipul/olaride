from django.shortcuts import render
from rest_framework import viewsets
from .models import Regions
# Create your views here.
from regions.serializers import RegionSerializer


class RegionViewSet(viewsets.ModelViewSet):
    queryset = Regions.objects.all()
    serializer_class = RegionSerializer