from rest_framework import viewsets
# Create your views here.
from .serializers import AddressSerializer
from .models import Addresses


class AddressViewSet(viewsets.ModelViewSet):
    queryset = Addresses.objects.all()
    serializer_class = AddressSerializer