from .models import Addresses
from rest_framework import serializers

__author__ = 'bipul'


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Addresses
        fields= ('id','title',)