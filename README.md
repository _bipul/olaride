# README #

Django Backend for ola ride app.

More API docs: 
https://docs.google.com/document/d/1JbZ1SIvbs0t837Y4Ck23rpswmVO1GST-1Olv661JADc/edit?usp=sharing

### What is this repository for? ###

* Quick summary
#olaappathon This is backend server for the OlaRide app for both drivers and customers. 
The app handles the Cab, Bus, Booking, Addresses, Drivers as a base.


* Version - v1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
Use virtualenv, after activating run pip install -r requirements.txt
* Configuration

* Dependencies
Django,
python-gcm
python-mysql
* Database configuration
Database:  mysql
user : root
database_name : olaride
* How to run tests
No tests
* Deployment instructions
Setup inside a virtual env, run with nginx, gunicorn FORT style setup

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
bipuljain44@gmail.com
* Other community or team contact