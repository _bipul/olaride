from addresses.serializers import AddressSerializer
from .models import Bookings, BookingRides
from rest_framework import serializers
from rides.models import CabRide, BusRide
from rides.serializers import CabRideSerializer, BusRideSerializer

__author__ = 'bipul'
class BookingRideRelatedField(serializers.RelatedField):
    """
    A custom field to use for the `tagged_object` generic relationship.
    """
    def to_representation(self, value):
        """
        Serialize bookmark instances using a bookmark serializer,
        and note instances using a note serializer.
        """
        if isinstance(value, CabRide):
            serializer = CabRideSerializer(value)
        elif isinstance(value, BusRide):
            serializer = BusRideSerializer(value)
        else:
            raise Exception('Unexpected type of tagged object')

        return serializer.data


class BookingRideSerializer(serializers.ModelSerializer):
    ride = BookingRideRelatedField(read_only=True)
    class Meta:
        model = BookingRides
        fields = ("id", 'ride',)


class BookingSerializer(serializers.ModelSerializer):
    pickup = AddressSerializer(read_only=True)
    destination = AddressSerializer(read_only=True)
    rides = BookingRideSerializer(many=True, read_only=True)

    class Meta:
        model = Bookings
        fields = ('id', 'pickup', 'destination', 'cost', 'status', 'created_on', 'rides')



