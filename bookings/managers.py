from django.db import models

__author__ = 'bipul'


class BookingManager(models.Manager):
    def get_for_id(self, id):
        return self.get(id=id, is_completed=False)

    def check_in(self, booking, ride_id):
        from bookings.views import push_msg

        from bookings.models import BookingRides

        booking_ride = BookingRides.objects.get(booking=booking, id=ride_id)
        msg = "RIDE CHECKED IN"
        if booking_ride.is_active:
            msg = "RIDE ALREADY CHECKED IN"
        else:
            push_msg("RIDE %s STARTED" % booking_ride.ride.number)
            booking_ride.is_active = True
            booking_ride.save()
        return msg

    def check_out(self, booking, ride_id):
        from bookings.views import push_msg

        from bookings.models import BookingRides
        booking_ride = BookingRides.objects.get(booking=booking, id=ride_id)
        msg = "RIDE CHECKED OUT"
        if not booking_ride.is_active:
            msg = "RIDE ALREADY CHECKED OUT"
        else:
            push_msg("RIDE %s STOPPED" % booking_ride.ride.number)
            booking_ride.is_active = False
            if booking_ride.order == booking.num_rides:
                push_msg("BOOKING %s COMPLETED" % booking.id)
                booking.is_completed = True
                booking.save()
            booking_ride.save()
        return msg