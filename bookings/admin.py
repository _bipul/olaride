from django.contrib import admin
from .models import Bookings,BookingRides

class BookingRideAdminInline(admin.TabularInline):
    model = BookingRides

class BookingAdmin(admin.ModelAdmin):
    inlines = [BookingRideAdminInline]


admin.site.register(Bookings, BookingAdmin)

# Register your models here.
