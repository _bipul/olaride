from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from addresses.models import Addresses
from bookings.managers import BookingManager


class Bookings(models.Model):
    user = models.ForeignKey(User)
    pickup = models.ForeignKey(Addresses, related_name='address_pickup')
    destination = models.ForeignKey(Addresses, related_name='address_destination')
    cost = models.PositiveIntegerField(default=0)
    status = models.BooleanField(default=False)
    last_modified = models.DateTimeField(auto_now_add=True)
    num_rides = models.PositiveIntegerField(default=0)
    is_completed = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now=True)
    objects = BookingManager()

    def __unicode__(self):
        return "booking : %d" % self.id

    class Meta:
        verbose_name = "Booking"
        verbose_name_plural = "Bookings"


class BookingRides(models.Model):
    booking = models.ForeignKey(Bookings, related_name='rides')
    ride_type = models.ForeignKey(ContentType)
    ride_id = models.PositiveIntegerField(default=0)
    ride = generic.GenericForeignKey(ct_field='ride_type', fk_field='ride_id')
    order = models.PositiveIntegerField(default=0)
    kms_run = models.PositiveIntegerField(default=0)
    is_active = models.BooleanField(default=False)
    last_modified = models.DateTimeField(auto_now_add=True)
    created_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "booking rides"

