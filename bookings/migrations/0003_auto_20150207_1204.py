# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
        ('bookings', '0002_auto_20150207_0951'),
    ]

    operations = [
        migrations.CreateModel(
            name='BookingRides',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ride_id', models.PositiveIntegerField(default=0)),
                ('last_modified', models.DateTimeField(auto_now_add=True)),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('booking', models.ForeignKey(to='bookings.Bookings')),
                ('ride_type', models.ForeignKey(blank=True, to='contenttypes.ContentType', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='bookings',
            name='ride_id',
        ),
        migrations.RemoveField(
            model_name='bookings',
            name='ride_type',
        ),
    ]
