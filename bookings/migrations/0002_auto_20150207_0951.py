# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('addresses', '0001_initial'),
        ('contenttypes', '0001_initial'),
        ('bookings', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookings',
            name='cost',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookings',
            name='destination',
            field=models.ForeignKey(related_name='address_destination', blank=True, to='addresses.Addresses', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookings',
            name='pickup',
            field=models.ForeignKey(related_name='address_pickup', blank=True, to='addresses.Addresses', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookings',
            name='ride_id',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookings',
            name='ride_type',
            field=models.ForeignKey(blank=True, to='contenttypes.ContentType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookings',
            name='status',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
