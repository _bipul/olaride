from django.contrib.auth.models import User
from gcm import GCM
from rest_framework import viewsets
# Create your views here.
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import BookingSerializer
from .models import Bookings
from rest_framework import status
from django.conf import settings


class BookingViewSet(viewsets.ModelViewSet):
    queryset = Bookings.objects.all()
    serializer_class = BookingSerializer


def push_msg(msg):
    gcm = GCM(settings.GCM_SECRET_KEY)
    data = {"action": msg}
    response = gcm.json_request(registration_ids=[settings.USER_DEV_ID], data=data)
    print response


class CheckInView(APIView):
    ## Starts the ride for the user
    def get(self, request, booking_id, ride_id):
        model = {}
        booking = Bookings.objects.get(id=booking_id)
        if booking.is_completed:
            model["msg"] = "Booking already completed"
        else:
            model["msg"] = Bookings.objects.check_in(booking, ride_id)
        return Response(model, status=status.HTTP_200_OK)


class CheckOutView(APIView):
    ## Starts the ride for the user
    def get(self, request, booking_id, ride_id):
        user = User.objects.get(username=settings.USER_NAME)
        # push_msg("Ride started")
        # gcm = GCM(settings.GCM_SECRET_KEY)
        # data = {"action": "status", "msg": "Ride started"}
        # response = gcm.json_request(registration_ids=[settings.USER_DEV_ID], data=data)
        booking = Bookings.objects.get(id=booking_id)
        model = {"msg": Bookings.objects.check_out(booking, ride_id)}
        return Response(model, status=status.HTTP_200_OK)




