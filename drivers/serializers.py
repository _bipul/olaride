from .models import Drivers
from rest_framework import serializers

__author__ = 'bipul'


class DriverSerializer(serializers.ModelSerializer):
    class Meta:
        model = Drivers
        fields = ('name',)
