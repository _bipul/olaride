from rest_framework import viewsets
# Create your views here.
from .serializers import DriverSerializer
from .models import Drivers


class DriverViewSet(viewsets.ModelViewSet):
    queryset = Drivers.objects.all()
    serializer_class = DriverSerializer