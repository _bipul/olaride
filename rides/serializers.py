from addresses.serializers import AddressSerializer
from drivers.serializers import DriverSerializer
from .models import BusRide, CabRide, BusRoutes
from rest_framework import serializers

__author__ = 'bipul'


class BusRouteSerializer(serializers.ModelSerializer):
    stop = AddressSerializer(read_only=True)

    class Meta:
        model = BusRoutes
        fields = ("stop", "order", "status")


class CabRideSerializer(serializers.ModelSerializer):
    driver = DriverSerializer(read_only=True)

    class Meta:
        model = CabRide
        fields = ("number", "driver")


class BusRideSerializer(serializers.ModelSerializer):
    driver = DriverSerializer(read_only=True)
    routes = BusRouteSerializer(many=True, read_only=True)

    class Meta:
        model = BusRide
        fields = ("number", "driver", "routes")
