# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('addresses', '0001_initial'),
        ('rides', '0004_remove_busride_driver_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='BusRoutes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.PositiveIntegerField(default=0)),
                ('status', models.BooleanField(default=False)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('bus', models.ForeignKey(to='rides.BusRide')),
                ('stop', models.ForeignKey(to='addresses.Addresses')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
