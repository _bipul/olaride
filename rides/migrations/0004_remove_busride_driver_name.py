# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rides', '0003_auto_20150207_0940'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='busride',
            name='driver_name',
        ),
    ]
