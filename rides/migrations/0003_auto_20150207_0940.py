# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('addresses', '0001_initial'),
        ('drivers', '0001_initial'),
        ('rides', '0002_auto_20150207_0931'),
    ]

    operations = [
        migrations.RenameField(
            model_name='busride',
            old_name='bus_no',
            new_name='number',
        ),
        migrations.RenameField(
            model_name='cabride',
            old_name='cab_no',
            new_name='number',
        ),
        migrations.RemoveField(
            model_name='cabride',
            name='driver_name',
        ),
        migrations.AddField(
            model_name='busride',
            name='driver',
            field=models.ForeignKey(blank=True, to='drivers.Drivers', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='busride',
            name='routes',
            field=models.ManyToManyField(to='addresses.Addresses'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cabride',
            name='driver',
            field=models.ForeignKey(blank=True, to='drivers.Drivers', null=True),
            preserve_default=True,
        ),
    ]
