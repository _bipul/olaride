from django.db import models


__author__ = 'bipul'


class BusRideManager(models.Manager):
    def check_in(self, bus, route_id):
        from bookings.views import push_msg
        route = bus.routes.get(stop_id=route_id)
        msg = "BUS CHECKED IN"
        if route.status:
            msg = "BUS ALREADY CHECKED IN"
        else:
            push_msg("check_n_bus")
            route.status = True
            route.save()
        return msg

