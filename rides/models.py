from django.db import models
from addresses.models import Addresses
# Create your models here.
from drivers.models import Drivers
from rides.managers import BusRideManager


class CabRide(models.Model):
    number = models.CharField(max_length=20)
    driver = models.ForeignKey(Drivers, null=True, blank=True)
    num_passengers = models.PositiveIntegerField(default=0)
    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.number


class BusRide(models.Model):
    number = models.CharField(max_length=20)
    driver = models.ForeignKey(Drivers, null=True, blank=True)
    num_passengers = models.PositiveIntegerField(default=0)
    is_active = models.BooleanField(default=True)
    objects =  BusRideManager()

    def __unicode__(self):
        return self.number


class BusRoutes(models.Model):
    bus = models.ForeignKey(BusRide, related_name="routes")
    stop = models.ForeignKey(Addresses, related_name="buses_stopped")
    order = models.PositiveIntegerField(default=0)
    status = models.BooleanField(default=False)
    last_modified = models.DateTimeField(auto_now=True)
    created_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return "Bus routes"