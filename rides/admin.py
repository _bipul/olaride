from django.contrib import admin
from .models import *

class BusRouteInline(admin.TabularInline):
    model = BusRoutes


class BusRideAdmin(admin.ModelAdmin):
    inlines = [BusRouteInline]


admin.site.register(CabRide)
admin.site.register(BusRide, BusRideAdmin)