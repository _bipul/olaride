from rest_framework import viewsets
# Create your views here.
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import *
from .models import *
from rest_framework import status


class CabRideViewSet(viewsets.ModelViewSet):
    queryset = CabRide.objects.all()
    serializer_class = CabRideSerializer

class BusRideViewSet(viewsets.ModelViewSet):
    queryset = BusRide.objects.all()
    serializer_class = BusRideSerializer


class BusCheckinView(APIView):
    def get(self, request, bus_id, address_id):
        model = {}
        bus = BusRide.objects.get(id=bus_id)
        model["msg"] = BusRide.objects.check_in(bus, address_id)
        return Response(model, status=status.HTTP_200_OK)
